# Hi everyone

This program can list all elements present in your bucket, you can upload a file , delete a given file and download it, i have used mime types s o that one can see jpeg images directly.

A flash message in blue will be displayed saying about the state of your actions i.e is it passed or failed.



Now to use this program you need to export several variables.

First Download all dependency using,

```
pip install -r requirements.txt;
```

```
1. export AWS_KEY= your aws secret key
2. export AWS_ID= your aws key id
3. export S3_BUCKET= your bucket name which will be used here.

```

Also you can run this using two options.

First ,
```
python3 main.py

```
But again this will not be best way to do. As you can't pass argument like host and port here.

Best Way
Again export one variable
export FLASK_APP=path to file main.py

Then run with
```
flask run
```
By default it runs on 127.0.0.1:5000, but you may want to run it on certain ip addresss and port and in order to do so.

```
flask run --host=192.168.0.111 --port=80
```
[Also check that port is not busy as well.]

Thanks 






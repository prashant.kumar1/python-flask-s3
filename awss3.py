import boto3
from config import AWS_ID,AWS_KEY,S3_BUCKET_NAME

s3= boto3.client (
    's3',
    aws_access_key_id=AWS_ID,
    aws_secret_access_key=AWS_KEY
);

def fetch_bucket_details():
    try :    
        s3_resource=boto3.resource('s3');
        my_bucket=s3_resource.Bucket(S3_BUCKET_NAME);
        return my_bucket;
    except Exception as e:
        print("Failed")
         
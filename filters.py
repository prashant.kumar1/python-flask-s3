import arrow
import os
import mimetypes

def datetimeformat(date):
    date_in_human_readable= arrow.get(date);
    return date_in_human_readable.humanize();

def  file_type(filename):
    file_info= os.path.splitext(filename);
    extension_name=file_info[1];
    try:
        return mimetypes.types_map[extension_name];
    except Exception as e:
        return "Unknown"; 
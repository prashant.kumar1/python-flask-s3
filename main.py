from flask import Flask,render_template,request,redirect,url_for,flash,Response
from flask_bootstrap import Bootstrap
from filters import datetimeformat,file_type
from awss3 import fetch_bucket_details 


app=Flask(__name__);
Bootstrap(app)

app.jinja_env.filters['date_in_readable'] = datetimeformat;
app.jinja_env.filters['file_type'] = file_type;

app.secret_key='secret_flask';


@app.route('/')
def index():
    return redirect(url_for('files'));

@app.route('/files')
def files():
    my_bucket=fetch_bucket_details();
    summaries= my_bucket.objects.all();
    return render_template( 'files.html' , my_bucket1=my_bucket ,files1=summaries  );

@app.route('/upload',methods=['POST'])
def upload():
    file = request.files['file']
    my_bucket=fetch_bucket_details();
    try :
       my_bucket.Object(file.filename).put(Body=file);
       flash('File Uploaded Successfully if file size is larger wait for few second or refresh link');
       return  redirect(url_for('files'));
    except Exception as e:
        flash("Failed to upload file");
        return  redirect(url_for('files'));

   

@app.route('/delete',methods=['POST'])
def delete():
    file = request.form['key']
    my_bucket=fetch_bucket_details();
    my_bucket.Object(file).delete();
    flash('File Deleted  Successfully');
    print(file)
    return  redirect(url_for('files'));

@app.route('/download',methods=['POST'])
def download():
    file = request.form['key']
    file_type = request.form['type']
    my_bucket=fetch_bucket_details();
    file_object = my_bucket.Object(file).get();

    return Response (
        file_object['Body'].read(),
        mimetype= file_type,
        headers={"Content-Disposition": "attachment:filename={}".format(file) }
    )
    return  redirect(url_for('files'));

if __name__== '__main__':
    app.run();

